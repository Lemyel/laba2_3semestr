﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Laba2_3semestr
{
    public static class GraphicsExtensions
    {
        public static void DrawCircle(this Graphics g, Pen pen, float centerX, float centerY, float Rmax, float Rmin)
        {
            g.DrawEllipse(pen, centerX - Rmax / 2, centerY - Rmax / 2, Rmax, Rmax);
            g.DrawEllipse(pen, centerX - Rmin / 2, centerY - Rmin / 2, Rmin, Rmin);

        }
        public static void FillCircle(this Graphics g, Brush brush, float centerX, float centerY, float Rmax, float Rmin)
        {
            g.FillEllipse(brush, centerX - Rmax / 2, centerY - Rmax / 2, Rmax, Rmax);
            g.FillEllipse(new SolidBrush(Color.White), centerX - Rmin / 2, centerY- Rmin / 2, Rmin, Rmin);
        }
    }
    public class Ring : GraphObject
    {

        public Ring(int x, int y) : base(x, y)
        {

        }
        public override void Draw(Graphics g)
        {
            g.FillCircle(brush, x, y, Rmax, Rmin);
            if (Selected) g.DrawCircle(inselected, x, y, Rmax, Rmin);
            else { g.DrawCircle(Pens.Black, x, y, Rmax, Rmin); }

        }
        public override bool ContainsPoint(Point p)
        {
            if ( ( (Math.Pow(p.X - X,2) / Math.Pow(Rmax/ 2, 2) + Math.Pow(p.Y - Y, 2) / Math.Pow(Rmax / 2, 2) )<= 1) && 
                (( Math.Pow(p.X - X, 2) / Math.Pow(Rmin / 2, 2) + Math.Pow(p.Y - Y, 2) / Math.Pow(Rmin / 2, 2)) > 1)) { return true; }
            else { return false; }
        }
    }
}
