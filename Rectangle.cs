﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Laba2_3semestr
{
    public class Rectangle : GraphObject
    {
        public Rectangle(int x, int y) : base(x, y)
        {

        }
        public override void Draw(Graphics g)
        {
            g.FillRectangle(brush, x-w/2, y-h/ 2, w, h);
            if (Selected) { g.DrawRectangle(inselected, x- w / 2, y- h / 2, w, h); }
            else { g.DrawRectangle(Pens.Black, x- w / 2, y- h / 2, w, h); }

        }
        public override bool ContainsPoint(Point p)
        {
            if ((p.X >= X-w/2) && (p.X <= X + w/2) && (p.Y >= Y-h/2) && (p.Y <= Y + h/2)) { return true; }
            else { return false; }
        }
    }
}
