﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laba2_3semestr
{
    public partial class Form1 : Form
    {
        static readonly Random r = new Random();
        static public Size MaxSize { get; set; }
        IGraphicFactory factory;
        public Form1()
        {            
            InitializeComponent();
            ComboBoxFactory.SelectedItem = "RandomObjectFactory";
            GraphObject.MaxSize = panel1.ClientSize;
        }

        readonly List<GraphObject> elements = new List<GraphObject>();
        private void PaintPanel(object sender, PaintEventArgs e)
        {
            foreach (GraphObject elem in elements)
            {
                elem.Draw(e.Graphics);
            }
        }
        public void panel1_ClientSizeChanged(object sender, EventArgs e)
        {
            GraphObject.MaxSize = panel1.ClientSize;//Getting Size of a form
        }

        private void Exit(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddFigure(object sender, EventArgs e)
        {
            try
            {
                elements.Add(factory.CreateGraphObject(r.Next(25, panel1.Width - 25), r.Next(25, panel1.Height - 25)));
                panel1.Invalidate();
#region
                /* int h = r.Next(100);
                 if (h < 50)
                 {
                     elements.Add(new Rectangle(r.Next(0, panel1.Width - 50), r.Next(0, panel1.Height - 50)));
                     panel1.Invalidate(); // чтобы позже вызывался Paint        
                 }
                 else
                 {
                     elements.Add(new Ellipse(r.Next(0, panel1.Width - 50), r.Next(0, panel1.Height - 50)));
                     panel1.Invalidate();
                 }*/
#endregion
                toolStripStatusLabel1.Text = "Добавлена фигура";
            }
            catch (ArgumentException exception)
            { MessageBox.Show(exception.Message); }
        }

        private void ClearFigures(object sender, EventArgs e)
        {
            elements.Clear();
            panel1.Invalidate();
        }
      
        private void Panel1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                GraphObject graphObject = new Ring(e.X, e.Y);
                elements.Add(graphObject);
                panel1.Invalidate();
#region
                /*int p = r.Next(10);
                if (p < 5)
                {
                GraphObject graphObject = new Ellipse(e.X - 25, e.Y - 25);
                panel1.Invalidate();// чтобы позже вызывался Paint        
                }
                else
                {
                    GraphObject graphObject = new Rectangle(e.X-25,e.Y-25);
                    elements.Add(graphObject);
                    panel1.Invalidate();
                }*/
#endregion
                toolStripStatusLabel1.Text = "X = "+e.X.ToString()+"; Y = "+e.Y.ToString();
            }
            catch (ArgumentException exception)
            { MessageBox.Show(exception.Message); }

        }

        private void Panel1_MouseDown(object sender, MouseEventArgs e)
        {
            int check = -1;
            int selectedEl = -1;
            for (int i = 0; i < elements.Count; ++i)
            {
                if (elements[i].Selected)
                {
                    selectedEl = i;
                }
                if (elements[i].ContainsPoint(e.Location))
                {
                    check = i;
                }
            }
            if (check != -1)
            {
                if (selectedEl != -1)
                {
                    for (int i = 0; i < elements.Count; i++) elements[i].Selected = false;
                }
                elements[check].Selected = true;
                panel1.Invalidate();
            }
            else
            {
                for (int i = 0; i < elements.Count; i++) elements[i].Selected = false;
                panel1.Invalidate();
            }
        }

        private void ClearFigure(object sender, EventArgs e)
        {
            for (int i = 0; i < elements.Count; i++)
            {
                if (elements[i].Selected) elements.RemoveAt(i);
            }
            panel1.Invalidate();
        }

        private void MoveFigure(object sender, EventArgs e)
        {
           
            try {
                int x =  r.Next(25, panel1.Width - 50);
                int y = r.Next(25, panel1.Height - 50);
                for (int i = 0; i < elements.Count; i++)
            {
                if (elements[i].Selected) elements[i].Move(x, y);
            }
            panel1.Invalidate();
            toolStripStatusLabel1.Text = "X = " + x.ToString() + "; Y = " + y.ToString();
            }
            catch (ArgumentException exception)
            { MessageBox.Show(exception.Message); }
        }

        private void ExitEsc(object sender, KeyEventArgs e)
        {
            if(e.KeyValue==(char)Keys.Escape)
             buttonExit.PerformClick();
        }

        private void ComboBoxFactory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((string)ComboBoxFactory.SelectedItem == "RandomObjectFactory")
            {
                factory = new RandomObjectFactory();
            }
            if ((string)ComboBoxFactory.SelectedItem == "TwoTypeFactory")
            {
                factory = new TwoTypeFactory();
            }
        }
    }
}

