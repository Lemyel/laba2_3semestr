﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba2_3semestr
{
    class RandomObjectFactory : IGraphicFactory
    {
        readonly Random r = new Random();
        public GraphObject CreateGraphObject(int width, int height)
        {
            int a = r.Next(10);
            if (a < 5)
            {
                return new Rectangle(width, height);
            }
            else
            {
                return new Ellipse(width, height);
            }
        }
    }
}
