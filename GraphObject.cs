﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Laba2_3semestr
{
    public abstract class GraphObject
    {
        static readonly Random r = new Random();
        protected int x, y, w, h, Rmin, Rmax;
        protected Color c;
        protected Pen inselected;
        protected SolidBrush brush;

        public GraphObject(int x, int y)
        {
            Color randomColor = Color.FromArgb(r.Next(256), r.Next(256), r.Next(256));
            c = randomColor;
            X = x;
            Y = y;
            w = 50;
            h = 50;
            Rmin = 30;
            Rmax = 50;
            brush = new SolidBrush(c);
            inselected = new Pen(Color.Red)
            {
                Width = 2
            };
        }
        public abstract bool ContainsPoint(Point p);
        public abstract void Draw(Graphics g);
        static public Size MaxSize { get; set; }
        public int X
        {
            get { return x; }
            set
            {
                if (value + 25 > MaxSize.Width) { throw new ArgumentException("Фигура выходит за границы"); }
                if (value -25< 0) { throw new ArgumentException("Фигура выходит за границы"); }
                x = value;
            }
        }
        public int Y
        {
            get { return y; }
            set
            {
                if (value + 25  > MaxSize.Height) { throw new ArgumentException("Фигура выходит за границы"); }
                if (value - 25 < 0) { throw new ArgumentException("Фигура выходит за границы"); }
                y = value;
            }
        }
        public bool Selected
        {
            get;
            set;
        }
        public void Move(int x, int y)
        {
            X = x;
            Y = y;
        }


    }
}