﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Laba2_3semestr
{
    public class Ellipse : GraphObject
    {
        public Ellipse(int x, int y) : base(x, y)
        {

        }
        public override void Draw(Graphics g)
        {
            g.FillEllipse(brush, x-w/2, y-h/2, w, h);
            if (Selected) { g.DrawEllipse(inselected, x- w / 2, y- h / 2, w, h); }
            else { g.DrawEllipse(Pens.Black, x- w / 2, y- h / 2, w, h); }

        }
        public override bool ContainsPoint(Point p)
        {
            if ((Math.Pow((p.X - X), 2) / Math.Pow(w / 2, 2) + Math.Pow((p.Y  - Y), 2) / Math.Pow(h / 2, 2)) <= 1) { return true; }
            else { return false; }
        }
    }
}
