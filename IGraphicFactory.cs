﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba2_3semestr
{
    interface IGraphicFactory
    {
        GraphObject CreateGraphObject(int width, int height);
    }
}
