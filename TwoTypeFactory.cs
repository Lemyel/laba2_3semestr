﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Laba2_3semestr
{
    class TwoTypeFactory: IGraphicFactory
    {
        bool a;
        public GraphObject CreateGraphObject(int width, int height)
        {
            if (!a)
            {
                a = true;
                return new Rectangle(width, height);
            }
            else
            {
                a = false;
                return new Ellipse(width, height);
            }
        }
    }
}
